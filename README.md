# Maze Clash

Mazeclash is an exhilarating online multipalyer maze game where two players race to the finish in a maze of their opponents creation.

Play me [**here**](http://mazeclash.herokuapp.com/)

If you have any ideas or improvements to suggest please do so [**here**](https://gitlab.com/matthewjk/maze-clash/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

## Running locally

Install node dependencies with: `npm install`

Start the react dev server with `npm run react` and start the NodeJS & Socket.io server with `npm run server`

Alternatively run both commans concurrently with: `npm start`
