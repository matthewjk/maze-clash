import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import io from "socket.io-client";

import "./index.css";

import MainMenu from "./components/mainmenu/MainMenu";

import LobbySearch from "./components/lobby/LobbySearch";
import LobbyCreate from "./components/lobby/LobbyCreate";
import LobbyView from "./components/lobby/LobbyView";
import LobbyJoin from "./components/lobby/LobbyJoin";

import Game from "./components/game/Game";

import NotFound from "./components/notfound/NotFound";

import MatchMake from "./components/matchmake/MatchMake";

var socketUrl;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { socket: null };
  }

  componentWillMount() {
    if (process.env.NODE_ENV === "production") {
      socketUrl = window.location.origin.replace(/^http/, "ws");
    } else {
      socketUrl = "http://localhost:8080";
    }
    this.initSocket();
  }

  initSocket() {
    const socket = io(socketUrl);
    this.setState({ socket });
  }

  render() {
    const { socket } = this.state;
    return (
      <Switch>
        <Route
          exact
          path="/"
          render={props => <MainMenu {...props} socket={socket} />}
        />
        <Route
          exact
          path="/matchmake"
          render={props => <MatchMake {...props} socket={socket} />}
        />
        <Route
          exact
          path="/lobby/search"
          render={props => <LobbySearch {...props} socket={socket} />}
        />
        <Route
          exact
          path="/lobby/create"
          render={props => <LobbyCreate {...props} socket={socket} />}
        />
        <Route
          path="/lobby/game/:lobby"
          render={props => <LobbyView {...props} socket={socket} />}
        />
        <Route
          path="/lobby/join/:lobby"
          render={props => <LobbyJoin {...props} socket={socket} />}
        />
        <Route
          path="/game/:gameid"
          render={props => <Game {...props} socket={socket} />}
        />
        <Route render={props => <NotFound {...props} socket={socket} />} />
      </Switch>
    );
  }
}

export default App;
