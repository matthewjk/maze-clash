import React, { Component } from "react";
// import { Link } from "react-router-dom";
import P5Wrapper from "react-p5-wrapper";
import Sketch from "./Sketch";

export default class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.setupgame = this.setupgame.bind(this);
  }

  componentWillMount() {
    this.setupgame(this.props);
  }

  componentWillUnmount() {
    this.props.history.goForward();
  }

  componentWillReceiveProps(newProps) {
    if (newProps !== undefined) {
      this.setupgame(newProps);
    }
  }

  setupgame(newProps) {
    const { socket } = this.props;
    this.setState({ socket: socket });

    var gameid = newProps.match.params.gameid;
    this.setState({ gameid: gameid });
    socket.emit("requestlobbyinfo", { lobby: gameid });
    socket.emit("requestboarddetails", { lobby: gameid });
    socket.on("lobbyinfo", data => {
      if (data !== null) {
        this.setState({ players: data.players });
        this.setState({ dimensions: data.difficulty });
        this.setState({ endpos: data.endpos });
      }
    });

    socket.on("gameredirect", () => {
      this.props.history.push("/lobby/game/" + this.state.gameid);
    });
    socket.on("unauthorised", data => {
      this.props.history.push("/lobby/search");
    });
  }

  render() {
    return (
      <div>
        {this.state.players && this.state.dimensions && this.state.endpos && (
          <P5Wrapper
            sketch={Sketch}
            socket={this.state.socket}
            players={this.state.players}
            dimensions={this.state.dimensions}
            endpos={this.state.endpos}
          />
        )}
      </div>
    );
  }
}
