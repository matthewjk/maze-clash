export default function sketch(p) {
  // Array of both players for into animation, passed as props
  var players = {};
  // Socket username
  var player;
  var boardwidth, boardheight;
  // Font passed as props
  var roboto;
  // Socket passed as props
  var socket;
  // Starting and ending postions for use by A* search
  var start, end;

  // Dimensions of the board change with difficulty and passed through props
  var rows, cols;
  // Ending postion needs to be uniform between clients, passed through props
  var endpos;

  // Dimensions for readout at the tops, width and height
  var infodimensions = [];
  var cellHeight, cellWidth;

  // Stuff needed for a* search
  var openset = [];
  var closedset = [];
  var path = [];
  var nosolution = false; // Search will be forced if no solution is found
  var grid = new Array(cols);

  // Timer has to be uniform between players hence cannt come from framecount and instead must come from server
  var timer;

  // Game controlled by state variable, can have one of the following states
  // ["INTRO_ANIMATION", "DRAW_MAZE", "CHECK_MAZE","CHECKING_OPPONENT", "SOLVE_MAZE","WINNER_DECLARED"]
  var state = "INTRO_ANIMATION";
  var winner, loser;

  // Textsize for into animation
  var tsize = 50;

  // opponent grid sent to opponent, playergrid recieved from opponent
  var opponentgrid = [];
  var playergrid = [];

  var lastcell = [];

  // Starts and resets A* if being run multiple times
  function ResetAStar() {
    if (!nosolution) {
      for (let i = 0; i < cols; i++) {
        grid[i] = new Array(rows);
      }

      for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
          grid[i][j] = new Cell(i, j);
        }
      }

      for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
          grid[i][j].addneighbours(grid);
          if (opponentgrid[i][j] === 1) {
            grid[i][j].wall = true;
          }
        }
      }
    }

    start = grid[cols - 1][Math.floor(rows / 2)];
    end = grid[0][endpos];
    start.player = true;
    end.end = true;
    player = start;
    start.wall = false;
    end.wall = false;

    nosolution = false;
    openset = [start];
    closedset = [];
    path = [];
  }

  function AStarSearch() {
    if (openset.length > 0) {
      var current = 0;
      for (let i = 0; i < openset.length; i++) {
        if (openset[i].f < openset[current].f) {
          current = i;
        }
      }
      current = openset[current];
      if (current === end) {
        // p.noLoop();
        console.log("FOUND THE END!");
        opponentgrid = reconstructgrid(grid);
        updategrid(opponentgrid, "opponent");
        state = "CHECKING_OPPONENT";
        socket.emit("mazechecked");
      }

      for (let i = openset.length - 1; i >= 0; i--) {
        if (openset[i] === current) {
          openset.splice(i, 1);
        }
      }

      closedset.push(current);

      var neighbours = current.neighbours;
      for (let i = 0; i < neighbours.length; i++) {
        var neighbour = neighbours[i];

        if (!closedset.includes(neighbour) && !neighbour.wall) {
          if (openset.includes(neighbour)) {
            if (current.g + 1 < neighbour.g) {
              neighbour.g = current.g + 1;
            }
          } else {
            neighbour.g = current.g + 1;
            openset.push(neighbour);
          }

          neighbour.h =
            Math.abs(neighbour.x - end.x) + Math.abs(neighbour.y - end.y);
          neighbour.f = neighbour.g + neighbour.h;

          neighbour.previous = current;
        }
      }
    } else {
      console.log("No solution, forcing");
      nosolution = true;

      if (closedset.length !== 1) {
        var lowest = closedset[1];
        for (let i = 0; i < closedset.length; i++) {
          if (
            (closedset[i].h > 0 && closedset[i].h < lowest.h) ||
            lowest.h === 0
          ) {
            lowest = closedset[i];
          }
        }
      } else {
        lowest = closedset[0];
      }
      lowest.clearwalls();
      ResetAStar();
      return;
    }

    if (!nosolution) {
      path = [];
      path.push(current);
      while (current.previous) {
        path.push(current.previous);
        current = current.previous;
      }
    }
  }

  function Cell(x, y) {
    this.x = x;
    this.y = y;

    this.f = 0;
    this.g = 0;
    this.h = 0;
    this.neighbours = [];
    this.wall = false;
    this.player = false;
    this.end = false;

    this.show = function(colour) {
      if (this.wall) {
        colour = 0;
      }
      if (this.player) {
        colour = p.color(255, 255, 0);
      }
      if (this.end) {
        colour = p.color(0, 255, 255);
      }

      p.fill(colour);
      p.strokeWeight(1);
      p.stroke(220, 220, 220);
      p.rect(
        this.x * cellWidth + boardwidth / 2,
        this.y * cellHeight,
        cellWidth,
        cellHeight
      );
      p.stroke(0, 0, 0);
    };

    this.addneighbours = function(grid) {
      if (this.x < cols - 1) {
        this.neighbours.push(grid[this.x + 1][this.y]);
      }
      if (this.x > 0) {
        this.neighbours.push(grid[this.x - 1][this.y]);
      }
      if (this.y < rows - 1) {
        this.neighbours.push(grid[this.x][this.y + 1]);
      }
      if (this.y > 0) {
        this.neighbours.push(grid[this.x][this.y - 1]);
      }
    };

    this.clearwalls = function() {
      for (let i = 0; i < this.neighbours.length; i++) {
        this.neighbours[i].wall = false;
        opponentgrid[this.neighbours[i].x][[this.neighbours[i].y]] = 0;
      }
    };
  }

  function reconstructgrid(grid) {
    let newgrid = [];
    for (let i = 0; i < cols; i++) {
      newgrid[i] = new Uint8Array(rows);
    }
    for (let i = 0; i < grid.length; i++) {
      for (let j = 0; j < grid[0].length; j++) {
        if (grid[i][j].wall) {
          newgrid[i][j] = 1;
        } else if (grid[i][j].player) {
          newgrid[i][j] = 2;
        } else if (grid[i][j].end) {
          newgrid[i][j] = 3;
        }
      }
    }
    return newgrid;
  }

  function Circle(x, y, d) {
    this.x = x;
    this.y = y;
    this.d = d;

    this.show = function() {
      p.circle(x, y, d);
    };
  }

  function drawgrid(g, pos) {
    let circles = [];
    let minrad = cellWidth < cellHeight ? cellWidth : cellHeight;
    let stylise = 20;

    p.stroke(220, 220, 220);
    for (let i = 0; i < g.length; i++) {
      for (let j = 0; j < Object.keys(g[0]).length; j++) {
        if (g[i][j] === 0) {
          p.fill(255, 255, 255);
          p.rect(
            i * cellWidth + (boardwidth * pos) / 2,
            j * cellHeight,
            cellWidth,
            cellHeight
          );
          p.fill(0);
        } else {
          var x = i * cellWidth + (boardwidth * pos) / 2 + cellWidth / 2;
          var y = j * cellHeight + cellHeight / 2;
          if (g[i][j] === 1) {
            //A little extra needs to be subtracted to prevent edges of circles clipping
            circles.push(new Circle(x, y, minrad - stylise - 3));
            continue;
          } else if (g[i][j] === 2) {
            p.fill(255, 0, 0);
            p.circle(x, y, minrad - stylise / 2);
            continue;
          } else if (g[i][j] === 3) {
            p.fill(0, 255, 255);
            p.circle(x, y, minrad - stylise / 2);
          }
        }
      }
    }
    p.fill(0, 0, 0);
    p.stroke(0);
    let l = circles.length;
    for (let i = 0; i < l; i++) {
      for (let j = 0; j < l; j++) {
        let circledist =
          Math.abs(circles[i].x - circles[j].x) +
          Math.abs(circles[i].y - circles[j].y);
        if (
          (circledist <= cellWidth + 10 || circledist <= cellHeight + 10) &&
          circledist !== 0
        ) {
          p.strokeWeight(minrad - stylise);
          p.line(circles[i].x, circles[i].y, circles[j].x, circles[j].y);
        }
      }
      p.strokeWeight(1);
      circles[i].show();
    }
    p.strokeWeight(1);
    p.fill(255, 255, 255);
  }

  function drawboard() {
    p.rectMode(p.CORNER);
    p.stroke(220, 220, 220);
    p.strokeWeight(1);
    // Make this only be redrawn if board update recieved
    drawgrid(playergrid, 0);
    drawgrid(opponentgrid, 1);
    if (state === "CHECK_MAZE") {
      p.strokeWeight(10);
      p.stroke(0, 0, 205);
      for (let i = 0; i < path.length; i++) {
        if (i >= 1) {
          let x1 = path[i].x * cellWidth + boardwidth / 2 + cellWidth / 2;
          let y1 = path[i].y * cellHeight + cellHeight / 2;
          let x2 = path[i - 1].x * cellWidth + boardwidth / 2 + cellWidth / 2;
          let y2 = path[i - 1].y * cellHeight + cellHeight / 2;
          p.line(x1, y1, x2, y2);
        }
      }
      p.stroke(0);
    }
  }

  function infobanner(time) {
    p.textSize(96);
    p.rectMode(p.CENTER);
    p.rect(
      boardwidth / 2,
      0,
      infodimensions[0] * cellWidth,
      infodimensions[1] * cellHeight
    );
    p.fill(0);
    p.textAlign(p.CENTER, p.TOP);
    if (state === "DRAW_MAZE") {
      p.text(time, boardwidth / 2, 0);
    } else if (state === "CHECK_MAZE") {
      p.text("Solving", boardwidth / 2, 0);
    } else if (state === "CHECKING_OPPONENT") {
      p.text("Waiting", boardwidth / 2, 0);
    } else if (state === "SOLVE_MAZE") {
      p.text("Race", boardwidth / 2, 0);
    } else if (state === "WINNER_DECLARED") {
      p.text("You win!", boardwidth / 2, 0);
    }
  }

  function moveplayer(xmove, ymove) {
    for (let i = 0; i < playergrid.length; i++) {
      for (let j = 0; j < playergrid[0].length; j++) {
        if (
          playergrid[i][j] === 2 &&
          i + xmove >= 0 &&
          j + ymove >= 0 &&
          i + xmove < playergrid.length &&
          j + ymove < playergrid[0].length
        ) {
          if (playergrid[i + xmove][j + ymove] !== 1) {
            if (playergrid[i + xmove][j + ymove] === 3) {
              socket.emit("endreached");
            }
            playergrid[i][j] = 0;
            playergrid[i + xmove][j + ymove] = 2;
            updategrid(playergrid, "player");
            return;
          }
        }
      }
    }
  }

  function drawcell(x, y, type) {
    let cellx = Math.floor((x - boardwidth / 2) / cellWidth);
    let celly = Math.floor(y / cellHeight);
    if (cellx !== lastcell[0] || celly !== lastcell[1] || type === "press") {
      if (cellx < cols && cellx >= 0 && celly < rows && celly >= 0) {
        let opponentcell = opponentgrid[cellx][celly];
        if (opponentcell !== 2 && opponentcell !== 3) {
          if (opponentcell === 0) {
            opponentgrid[cellx][celly] = 1;
          } else {
            opponentgrid[cellx][celly] = 0;
          }
          lastcell = [cellx, celly];
        }
        updategrid(opponentgrid, "opponent");
      }
    }
  }

  function updategrid(opponentgrid, player) {
    let buffer = opponentgrid.map(x =>
      x.buffer.slice(x.byteOffset, x.byteOffset + x.byteLength)
    );
    socket.emit("updateboard", { board: buffer, player: player });
  }

  function initSockets() {
    socket.emit("getusername");
    socket.on("username", data => {
      player = data;
    });

    socket.on("opponentboard", data => {
      playergrid = data.map(x => new Uint8Array(x));
      playergrid = playergrid.reverse();
    });

    socket.on("playerboard", data => {
      opponentgrid = data.map(x => new Uint8Array(x));
      opponentgrid = opponentgrid.reverse();
    });

    socket.on("timer", data => {
      timer = data;
      if (timer <= 0) {
        ResetAStar();
        state = "CHECK_MAZE";
      }
    });

    socket.on("solvemaze", () => {
      state = "SOLVE_MAZE";
    });

    socket.on("winnerdeclared", data => {
      winner = data.winner;
      loser = data.loser;
      tsize = 50;
      state = "WINNER_DECLARED";
      console.log("The winner is: " + winner);
    });
  }

  function introanimation() {
    // Diagonal line
    p.fill(255);
    p.stroke(0);
    p.strokeWeight(7.5);
    p.line(boardwidth, 0, 0, boardheight);

    // Central VS
    p.fill(255);
    p.stroke(255);
    p.rectMode(p.CENTER);
    p.rect(boardwidth / 2, boardheight / 2, 100, 100);
    p.textAlign(p.CENTER, p.CENTER);
    p.textSize(64);
    p.fill(0);
    p.text("VS", boardwidth / 2, boardheight / 2);

    //Player text
    p.textSize(tsize);
    p.text(players[0], boardwidth / 4, boardheight / 4);
    p.text(players[1], 3 * (boardwidth / 4), 3 * (boardheight / 4));
  }

  function winneranimation() {
    p.fill(0);
    p.stroke(0);
    p.strokeWeight(1);
    p.textAlign(p.CENTER, p.CENTER);

    p.textSize(tsize);
    p.text(winner + " wins!", boardwidth / 2, boardheight / 4);
    p.textSize(75 - tsize / 2);
    p.text(loser + " loses!", boardwidth / 2, 3 * (boardheight / 4));
  }

  // Props are passed from game container here
  p.myCustomRedrawAccordingToNewPropsHandler = function(props) {
    socket = props.socket;
    initSockets();
    players = props.players;
    cols = props.dimensions[0];
    rows = props.dimensions[1];
    infodimensions = [Math.floor(cols / 1.5), Math.floor(rows / 3)];
    endpos = props.endpos;
  };

  p.preload = function() {
    roboto = p.loadFont(
      process.env.PUBLIC_URL + "/assets/fonts/Roboto-Light.ttf"
    );
  };

  p.setup = function() {
    boardwidth = p.windowWidth * 0.75;
    boardheight = p.windowHeight * 0.95;
    cellWidth = boardwidth / 2 / cols;
    cellHeight = boardheight / rows;
    p.frameRate(30);
    p.createCanvas(boardwidth, boardheight);
    p.textFont(roboto);

    for (let i = 0; i < cols; i++) {
      opponentgrid[i] = new Uint8Array(rows);
      playergrid[i] = new Uint8Array(rows);
    }

    for (let i = 0; i < opponentgrid.length; i++) {
      for (let j = 0; j < Object.keys(opponentgrid).length; j++) {
        if (i < infodimensions[0] / 2 && j < infodimensions[1] / 2) {
          opponentgrid[i][j] = 1;
          playergrid[cols - i - 1][j] = 1;
        }
      }
    }

    opponentgrid[cols - 1][Math.floor(rows / 2)] = 2;
    opponentgrid[0][endpos] = 3;
    playergrid[0][Math.floor(rows / 2)] = 2;
    playergrid[cols - 1][endpos] = 3;
  };

  p.draw = function() {
    p.background(255, 255, 255);
    if (state === "INTRO_ANIMATION") {
      if (tsize <= 100) {
        tsize += 0.5;
        introanimation();
      } else {
        state = "DRAW_MAZE";
      }
    } else if (state === "WINNER_DECLARED") {
      if (tsize <= 100) {
        tsize += 0.5;
        winneranimation();
      } else {
        p.noLoop();
        socket.emit("requestgameredirect");
      }
    } else {
      if (state === "CHECK_MAZE") {
        AStarSearch();
      }

      drawboard();
      p.strokeWeight(7.5);
      p.line(boardwidth / 2, 0, boardwidth / 2, endpos * cellHeight);
      p.line(
        boardwidth / 2,
        (endpos + 1) * cellHeight,
        boardwidth / 2,
        boardheight
      );
      p.stroke(0, 255, 255);
      p.line(
        (cols - 1) * cellWidth + cellWidth / 2,
        endpos * cellHeight + cellHeight / 2,
        boardwidth - (cols - 1) * cellWidth - cellWidth / 2,
        endpos * cellHeight + cellHeight / 2
      );
      p.stroke(0);
      infobanner(timer);
    }
  };

  p.keyPressed = function() {
    if (p.keyCode === 13) {
      ResetAStar();
      state = "CHECK_MAZE";
    }
    if (state === "SOLVE_MAZE") {
      if (p.keyCode === 37) {
        moveplayer(-1, 0);
      }
      if (p.keyCode === 38) {
        moveplayer(0, -1);
      }
      if (p.keyCode === 39) {
        moveplayer(1, 0);
      }
      if (p.keyCode === 40) {
        moveplayer(0, 1);
      }
    }
  };

  p.mouseDragged = function() {
    if (state === "DRAW_MAZE") {
      drawcell(p.mouseX, p.mouseY, "drag");
    }
  };

  p.mousePressed = function() {
    if (state === "DRAW_MAZE") {
      drawcell(p.mouseX, p.mouseY, "press");
    }
  };
}
