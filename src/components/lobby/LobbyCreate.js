import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class LobbyCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.createLobby = this.createLobby.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.incorrectinput = this.incorrectinput.bind(this);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleEnter, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleEnter, false);
  }

  handleEnter(event) {
    if (event.keyCode === 13) {
      this.createLobby();
    }
  }

  incorrectinput(elementid) {
    let element = document.getElementById(elementid);
    element.classList.add("flashincorrect");
    setTimeout(() => {
      element.classList.remove("flashincorrect");
    }, 3000);
  }

  createLobby() {
    var lobbyname = document.getElementById("lobbyname").value;
    lobbyname = lobbyname.replace(/[.,/#!$%^&*;:{}=\-_`~()]/g, "");
    lobbyname = lobbyname.replace(/\s{2,}/g, "");
    lobbyname = lobbyname.replace(/\s/g, "");
    lobbyname = lobbyname.trim();
    // REGEX TO FILTER OUT WIERD CHARS

    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let difficulty = document.getElementById("difficultyselect").value;

    if (username === "" || username.length >= 15) {
      this.incorrectinput("username");
      return;
    }
    if (lobbyname === "" || lobbyname.length >= 15) {
      this.incorrectinput("lobbyname");
      return;
    }

    const { socket } = this.props;

    let data = {
      lobbyname: lobbyname,
      username: username,
      password: password,
      difficulty: difficulty
    };

    socket.emit("createlobby", data);

    socket.on("lobbyexists", () => {
      this.incorrectinput("lobbyname");
    });

    socket.on("createdsuccessfully", () => {
      socket.removeListener("createdsuccessfully");
      socket.emit("joinlobby", data);
    });

    socket.on("joinedsuccessfully", () => {
      this.props.history.push("/lobby/game/" + lobbyname);
    });

    socket.on("incorrectpassword", () => {
      console.log("Incorrect password provided");
    });
  }

  render() {
    return (
      <div className="outline center">
        <div className="menutitle">Creating Lobby</div>
        <div className="menutext menuitem">
          <div className="label">Username:</div>
          <input
            id="username"
            className="inputmin"
            type="text"
            name="username"
          />
        </div>
        <br />
        <div className="menutext menuitem">
          <div className="label">Lobby name:</div>
          <input
            id="lobbyname"
            className="inputmin"
            type="text"
            name="lobbyname"
          />
        </div>
        <br />
        <div className="menutext menuitem">
          <div className="label">Password:</div>
          <input
            id="password"
            className="inputmin"
            type="password"
            name="password"
          />
        </div>
        <br />
        <div className="menuitem">
          <div className="menutext label">Difficulty:</div>
          <div className="minselect">
            <select id="difficultyselect" defaultValue="Medium">
              <option value="Easy">Easy</option>
              <option value="Medium">Medium</option>
              <option value="Hard">Hard</option>
            </select>
          </div>
        </div>
        <br />
        <div className="menuitem">
          <Link className="buttonlink" to="/lobby/search">
            <div className="menubutton">Back</div>
          </Link>
          <a className="buttonlink" onClick={this.createLobby}>
            <div className="menubutton">Create</div>
          </a>
        </div>
      </div>
    );
  }
}
