import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class LobbyJoin extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.joinLobby = this.joinLobby.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.incorrectinput = this.incorrectinput.bind(this);
    this.verifylobby = this.verifylobby.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps !== undefined) {
      this.verifylobby(newProps);
    }
  }
  componentWillMount() {
    this.verifylobby(this.props);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleEnter, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleEnter, false);
  }

  handleEnter(event) {
    if (event.keyCode === 13) {
      this.joinLobby();
    }
  }

  verifylobby(newProps) {
    const { socket } = this.props;
    var lobbyid = newProps.match.params.lobby;
    socket.emit("verifylobby", { lobby: lobbyid });

    socket.on("unauthorised", data => {
      this.props.history.push("/lobby/search");
    });
  }

  incorrectinput(elementid) {
    let element = document.getElementById(elementid);
    element.classList.add("flashincorrect");
    setTimeout(() => {
      element.classList.remove("flashincorrect");
    }, 3000);
  }

  joinLobby() {
    const { socket } = this.props;

    var lobbyname = this.props.match.params.lobby;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if (username === "" || username.length >= 15) {
      this.incorrectinput("username");
      console.log("You need to enter a lobby name and username");
      return;
    }

    var data = {
      lobbyname: lobbyname,
      username: username,
      password: password
    };
    socket.emit("joinlobby", data);
    socket.on("joinedsuccessfully", () => {
      this.props.history.push("/lobby/game/" + lobbyname);
    });
    socket.on("incorrectpassword", () => {
      console.log("Incorrect password provided");
      this.incorrectinput("password");
    });
  }

  render() {
    return (
      <div className="outline center">
        <div className="menutitle">Joining Lobby</div>
        <div className="menutext menuitem">
          <div className="label">Username:</div>
          <input
            id="username"
            className="inputmin"
            type="text"
            name="username"
          />
        </div>
        <br />

        <div className="menutext menuitem">
          <div className="label">Password:</div>
          <input
            id="password"
            className="inputmin"
            type="password"
            name="password"
          />
        </div>
        <br />
        <div className="menuitem">
          <Link className="buttonlink" to="/lobby/search">
            <div className="menubutton">Back</div>
          </Link>
          <a onClick={this.joinLobby} className="buttonlink">
            <div className="menubutton">Join</div>
          </a>
        </div>
      </div>
    );
  }
}
