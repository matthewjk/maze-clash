import React, { Component } from "react";
import { Link } from "react-router-dom";

import lock from "../../assets/img/lock.svg";
import full from "../../assets/img/full.svg";
import inprogress from "../../assets/img/inprogress.svg";

export default class LobbySearch extends Component {
  constructor(props) {
    super(props);
    this.state = { openlobbys: [] };
    this.setuplobbyinfo = this.setuplobbyinfo.bind(this);
    this.noopenlobbys = this.noopenlobbys.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setuplobbyinfo(this.props);
    }
  }

  componentWillMount() {
    this.setuplobbyinfo(this.props);
  }

  componentWillReceiveProps(newProps) {
    if (newProps !== undefined) {
      this.setuplobbyinfo(newProps);
    }
  }

  setuplobbyinfo(newProps) {
    const { socket } = this.props;
    socket.emit("requestopenlobbys");
    socket.on("openlobbys", data => {
      this.setState({ openlobbys: data });
    });
  }

  noopenlobbys() {
    for (let i in this.state.openlobbys) {
      if (this.state.openlobbys[i].matchmade == false) {
        return false;
      }
    }
    return true;
  }

  render() {
    const lobbys = this.state.openlobbys;
    // Change this so that if there are only matchmade games nolobbys also returns true
    const nolobbys = this.noopenlobbys();
    const lobbynames = Object.keys(this.state.openlobbys).map(function(d) {
      if (lobbys[d].matchmade === false) {
        return (
          <Link
            key={d}
            to={lobbys[d].players.length >= 2 ? null : `/lobby/join/${d}`}
          >
            <div>
              {d}
              {lobbys[d].password !== "" && (
                <img
                  src={lock}
                  className="svgicon"
                  alt="Password"
                  title="Password is required to join"
                />
              )}
              {lobbys[d].players.length >= 2 && (
                <img
                  src={full}
                  className="svgicon"
                  alt="Full"
                  title="Game is full"
                />
              )}
              {lobbys[d].inprogress === true && (
                <img
                  src={inprogress}
                  className="svgicon"
                  alt="Inprogress"
                  title="Game is in progress"
                />
              )}
            </div>
          </Link>
        );
      }
    });

    return (
      <div className="outline center">
        <div style={{ paddingTop: "5px" }} className="menutitle">
          Lobby search
        </div>
        <div className="horizbreak" />
        <div id="lobbylist" className="minlist">
          {nolobbys ? (
            <div id="nolobbys" className="menutext">
              No open lobbys
            </div>
          ) : (
            <div id="lobbys">{lobbynames}</div>
          )}
        </div>
        <div className="horizbreak" style={{ marginBottom: " 25px" }} />
        <div className="menuitem">
          <Link className="buttonlink" to="/">
            <div className="menubutton">Back</div>
          </Link>
          <Link className="buttonlink" to="/lobby/create">
            <div className="menubutton">Create Lobby</div>
          </Link>
        </div>
      </div>
    );
  }
}
