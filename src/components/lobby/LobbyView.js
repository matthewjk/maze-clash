import React, { Component } from "react";
// import { Link } from "react-router-dom";

export default class LobbyView extends Component {
  constructor(props) {
    super(props);
    this.state = { players: [], ready: [] };
    this.leavelobby = this.leavelobby.bind(this);
    this.ready = this.ready.bind(this);
    this.setuplobby = this.setuplobby.bind(this);
  }

  componentWillMount() {
    this.setuplobby(this.props);
  }

  componentWillReceiveProps(newProps) {
    if (newProps !== undefined) {
      this.setuplobby(newProps);
    }
  }

  setuplobby(newProps) {
    const { socket } = this.props;
    let lobby = newProps.match.params.lobby;
    this.setState({ lobby: lobby });
    socket.emit("requestlobbyinfo", { lobby: lobby });
    socket.on("lobbyinfo", data => {
      if (data !== null) {
        this.setState({ players: data.players });
        this.setState({ ready: data.ready });
      }
    });
    socket.on("unauthorised", data => {
      this.props.history.push("/lobby/search");
    });
    socket.on("startinggame", () => {
      this.props.history.push("/game/" + this.state.lobby);
    });
  }

  leavelobby() {
    const { socket } = this.props;
    let lobby = this.state.lobby;
    socket.emit("lobbydisconnect", { lobby: lobby });
    this.props.history.push("/");
  }

  ready() {
    const { socket } = this.props;
    let lobby = this.state.lobby;
    socket.emit("requestplay", { lobby: lobby });
    socket.emit("requestlobbyinfo", { lobby: lobby });
  }

  render() {
    const ready = this.state.ready;
    const players = this.state.players.map(d => (
      <li key={d}>
        {ready.indexOf(d) !== -1 ? (
          <span className="ready">{d}</span>
        ) : (
          <span>{d}</span>
        )}
      </li>
    ));
    return (
      <div className="outline center">
        <div id="lobbytitle" className="menutitle loading">
          {this.state.players.length < 2
            ? "Waiting for players"
            : "Waiting to start"}
        </div>
        <ul id="playerlist" className="minlist">
          {players}
        </ul>
        <div className="menuitem">
          <a className="buttonlink" onClick={this.leavelobby}>
            <div className="menubutton">Leave</div>
          </a>
          <a className="buttonlink" onClick={this.ready}>
            <div className="menubutton">Ready</div>
          </a>
        </div>
      </div>
    );
  }
}
