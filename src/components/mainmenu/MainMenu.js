import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { socket: null };
  }

  componentDidMount() {
    this.setState({ socket: this.props.socket });
  }

  render() {
    return (
      <div className="outline center">
        <div className="menutitle">Maze Clash</div>
        <Link className="buttonlink" to="/matchmake">
          <div className="menubutton">Matchmaking</div>
        </Link>
        <br />
        <Link className="buttonlink" to="/lobby/search">
          <div className="menubutton">Lobby</div>
        </Link>
      </div>
    );
  }
}
