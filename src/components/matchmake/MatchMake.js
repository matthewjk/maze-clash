import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class MatchMake extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.incorrectinput = this.incorrectinput.bind(this);
    this.matchmake = this.matchmake.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleEnter, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleEnter, false);
  }

  handleEnter(event) {
    if (event.keyCode === 13) {
      this.matchmake();
    }
  }

  incorrectinput(elementid) {
    let element = document.getElementById(elementid);
    element.classList.add("flashincorrect");
    setTimeout(() => {
      element.classList.remove("flashincorrect");
    }, 3000);
  }

  matchmake() {
    let username = document.getElementById("username").value;
    if (username === "" || username.length >= 15) {
      this.incorrectinput("username");
      return;
    } else {
      console.log("Matchmaking");
      const { socket } = this.props;
      socket.emit("requestmatchmake");

      socket.on("matchmadesuccessfully", data => {
        socket.removeListener("createdsuccessfully");
        socket.on("joinedsuccessfully", () => {
          this.props.history.push("/lobby/game/" + data.lobbyname);
        });

        socket.emit("joinlobby", {
          username: username,
          lobbyname: data.lobbyname,
          password: ""
        });
      });
    }
  }

  render() {
    return (
      <div className="outline center">
        <div className="menutitle">Matchmaking</div>
        <div className="menutext menuitem">
          <div className="label">Username:</div>
          <input
            id="username"
            className="inputmin"
            type="text"
            name="username"
          />
        </div>
        <br />
        <div className="menuitem">
          <Link className="buttonlink" to="/">
            <div className="menubutton">Back</div>
          </Link>
          <a onClick={this.matchmake} className="buttonlink">
            <div className="menubutton">Start</div>
          </a>
        </div>
      </div>
    );
  }
}
