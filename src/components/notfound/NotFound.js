import React, { Component } from "react";
// import { Link } from "react-router-dom";

export default class NotFound extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="outline center">
        <div className="menutitle">404, Redirecting</div>
      </div>
    );
  }
}
