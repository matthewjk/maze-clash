const io = require("./index.js").io;

var clients = {};
var difficulty = {
  Easy: [10, 18],
  Medium: [16, 25],
  Hard: [26, 35]
};

var lobbys = {};

module.exports = function(socket) {
  console.log("Socket connected, id: " + socket.id);

  socket.on("requestlobbys", data => {
    io.emit("openlobbys", rooms);
  });

  socket.on("requestmatchmake", data => {
    console.log("requested matchmake");
    let matchmadelobby;
    for (let i in lobbys) {
      if (
        lobbys[i].matchmade === true &&
        lobbys[i].players.length < 2 &&
        lobbys[i].inprogress === false
      ) {
        matchmadelobby = i;
        break;
      }
    }
    if (matchmadelobby) {
      console.log("Found existing lobby, joining");
      io.to(socket.id).emit("matchmadesuccessfully", {
        lobbyname: matchmadelobby
      });
    } else {
      let now = new Date();
      now = now.getTime();
      let lobbyname = now + socket.id;
      var endpos = Math.floor(
        Math.random() * (difficulty["Medium"][0] - 4 + 1) + 4
      );
      lobbys[lobbyname] = {
        players: [],
        password: "",
        matchmade: true,
        difficulty: difficulty["Medium"],
        endpos: endpos,
        inprogress: false,
        mazechecked: [],
        ready: []
      };
      io.to(socket.id).emit("matchmadesuccessfully", { lobbyname });
    }
  });

  socket.on("createlobby", data => {
    if (!(data.lobbyname in lobbys)) {
      var endpos = Math.floor(
        Math.random() * (difficulty[data.difficulty][0] - 4 + 1) + 4
      );
      lobbys[data.lobbyname] = {
        players: [],
        password: data.password,
        matchmade: false,
        difficulty: difficulty[data.difficulty],
        endpos: endpos,
        inprogress: false,
        mazechecked: [],
        ready: []
      };
      console.log("Room/Password : " + data.lobbyname + "/" + data.password);
      io.to(socket.id).emit("createdsuccessfully");
    } else {
      io.to(socket.id).emit("lobbyexists");
    }
  });

  socket.on("verifylobby", data => {
    if (!(data.lobby in lobbys)) {
      io.to(socket.id).emit("unauthorised");
    } else if (lobbys[data.lobby].players.length >= 2) {
      io.to(socket.id).emit("unauthorised");
    }
  });

  socket.on("joinlobby", data => {
    socket.username = data.username;
    socket.lobbyname = data.lobbyname;
    socket.password = data.password;
    if (
      lobbys[socket.lobbyname].password === socket.password &&
      lobbys[socket.lobbyname].players.indexOf(socket.username) == -1
    ) {
      socket.join(socket.lobbyname);
      lobbys[socket.lobbyname].players.push(socket.username);
      io.to(socket.id).emit("joinedsuccessfully");
    } else {
      io.to(socket.id).emit("incorrectpassword");
    }
  });

  socket.on("requestlobbyinfo", data => {
    if (socket.username == undefined || socket.lobbyname == undefined) {
      io.to(socket.id).emit("unauthorised");
    } else {
      io.in(socket.lobbyname).emit("lobbyinfo", lobbys[data.lobby]);
    }
  });

  socket.on("requestopenlobbys", data => {
    io.to(socket.id).emit("openlobbys", lobbys);
  });

  socket.on("disconnect", data => {
    console.log("Socket disconnected, id: " + socket.id);
    disconnect();
  });

  socket.on("lobbydisconnect", data => {
    disconnect();
  });

  function disconnect() {
    if (socket.lobbyname && lobbys[socket.lobbyname]) {
      var arr = lobbys[socket.lobbyname].players;
      var index = arr.indexOf(socket.username);
      if (index >= -1) {
        lobbys[socket.lobbyname].players.splice(index, 1);
        io.in(socket.lobbyname).emit("lobbyinfo", lobbys[socket.lobbyname]);
        if (lobbys[socket.lobbyname].inprogress == true) {
          io.in(socket.lobbyname).emit("winnerdeclared", {
            winner: lobbys[socket.lobbyname].players[0],
            loser: socket.username
          });
        }
      }
      if (lobbys[socket.lobbyname].players.length == 0) {
        delete lobbys[socket.lobbyname];
        if (typeof timer !== "undefined") {
          clearInterval(timer);
        }

        io.emit("openlobbys", lobbys);
      }
    }
    delete socket.lobbyname;
    delete socket.username;
  }

  socket.on("requestusername", () => {
    io.to(socket.id).emit("username", socket.username);
  });

  // socket.on("reset", function(data) {
  //   countdown = 1000;
  //   io.in(socket.lobbyname).emit("timer", { countdown: countdown });
  // });

  socket.on("getusername", data => {
    socket.emit("username", socket.username);
  });

  // if the player changes the maze or makes a move the whole board will be updated
  socket.on("updateboard", data => {
    // io.in(socket.lobbyname).emit("opponentboard", data);
    if (data.player === "player") {
      socket.to(socket.lobbyname).emit("playerboard", data.board);
    }

    if (data.player === "opponent") {
      socket.to(socket.lobbyname).emit("opponentboard", data.board);
    }
  });

  socket.on("mazechecked", data => {
    let checkedplayers = lobbys[socket.lobbyname].mazechecked;
    if (!checkedplayers.includes(socket.username)) {
      lobbys[socket.lobbyname].mazechecked.push(socket.username);
    }
    if (
      lobbys[socket.lobbyname].mazechecked.sort().join(",") ==
      lobbys[socket.lobbyname].players.sort().join(",")
    ) {
      console.log("Both mazes checked");
      io.in(socket.lobbyname).emit("solvemaze");
    }
  });

  socket.on("endreached", () => {
    let players = lobbys[socket.lobbyname].players.slice();
    players.splice(players.indexOf(socket.username), 1);
    io.in(socket.lobbyname).emit("winnerdeclared", {
      winner: socket.username,
      loser: players[0]
    });
  });

  socket.on("requestgameredirect", () => {
    lobbys[socket.lobbyname].inprogress = false;
    io.in(socket.lobbyname).emit("gameredirect");
  });

  socket.on("requestplay", data => {
    if (lobbys[data.lobby].ready.indexOf(socket.username) == -1) {
      lobbys[data.lobby].ready.push(socket.username);
      console.log("Adding player to ready");
      if (
        lobbys[data.lobby].ready.sort().join(",") ==
        lobbys[data.lobby].players.sort().join(",")
      ) {
        lobbys[data.lobby].ready = [];
        lobbys[data.lobby].inprogress = true;
        io.in(data.lobby).emit("startinggame");

        var duration = 63;
        var timer = setInterval(function() {
          io.in(socket.lobbyname).emit("timer", duration);
          if (duration <= 0) {
            clearInterval(timer);
          }
          duration--;
        }, 1000);
      }
    }
  });

  socket.on("notplayingagain", data => {
    let usernameindex = lobbys[data.lobby].ready.indexOf(socket.username);
    if (usernameindex != -1) {
      lobbys[data.lobby].ready.splice(usernameindex, 1);
    }
    disconnect();
  });
};
