var path = require("path");
var compression = require("compression");
const express = require("express");
const favicon = require("express-favicon");
const app = require("express")();
const server = require("http").Server(app);
var io = (module.exports.io = require("socket.io")(server));
const SocketManager = require("./SocketManager");
const PORT = process.env.PORT || 8080;
var approot = process.env.PWD;
io.on("connection", SocketManager);

if (process.env.NODE_ENV == "production") {
  app.use(compression());
  app.use(favicon(approot + "/build/favicon.ico"));
  app.use(express.static(approot));
  app.use(express.static(path.join(approot, "build")));
  app.get("/*", function(req, res) {
    res.sendFile(path.join(approot, "build", "index.html"));
  });
  app.get("/ping", function(req, res) {
    return res.send("pong");
  });
}
server.listen(PORT, () => {
  if (process.env.NODE_ENV == "production") {
    console.log("Production server running on port: " + PORT);
  } else {
    console.log("Starting in development mode");
    console.log("Websockets running on port: " + PORT);
  }
});
